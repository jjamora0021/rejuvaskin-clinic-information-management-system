<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TimekeepingModel;

class TimekeepingController extends Controller
{
	/**
	 * [__construct description]
	 */
    public function __construct()
    {
    	$this->TimekeepingModel = new \App\TimekeepingModel;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
    	return view('pages.timekeeping.timekeeping-page');
    }
}
